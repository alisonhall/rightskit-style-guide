<aside class="sidebar">
	<header id="home">
		<img class="logo" src="images/icon_300x.png" alt="Rights.Kit logo">
		<img class="lettermark" src="images/rightskit_lettermark2cs6.png" alt="Rights.Kit">
	</header>
	<nav>
		<ul>
			<?php
				global $sidebar;
				echo $sidebar;
			?>
		</ul>
	</nav>
</aside>