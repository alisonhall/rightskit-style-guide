        <?php 
        	include("head.php");
        	include("functions.php");
        	include("data.php");
        	$sidebar = '';
        ?>
		
		<section class="content">
			<h1>Rights.Kit Front-End Style Guide</h1>
			<?php 



				/**********************************
				STYLES SECTION
				***********************************/

				$sectionID = "styles";
				$sectionTitle = "Styles";
				$sectionDescription = "These are the general guidelines for what the style of Rights.Kit should be.";
				createSection($sectionID, $sectionTitle, $sectionDescription);

					$itemID = "colours";
					$itemTitle = "Colour Palette";
					$itemDescription = "The following are the official Rights.Kit branding colours.";
					$itemHTML = $coloursHTML;
					$itemCSS = $coloursCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "coloursLighter";
					$itemTitle = "Colour Palette - Lighter";
					$itemDescription = "The lighter version of the branding colours";
					$itemHTML = $coloursLighterHTML;
					$itemCSS = $coloursLighterCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "coloursDarker";
					$itemTitle = "Colour Palette - Darker";
					$itemDescription = "The darker version of the branding colours";
					$itemHTML = $coloursDarkerHTML;
					$itemCSS = $coloursDarkerCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "typeface";
					$itemTitle = "Typeface Styles";
					$itemDescription = "Rights.Kit typeface: Slim Joe and Source Sans Pro";
					$itemHTML = $typefaceHTML;
					$itemCSS = $typefaceCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "tooltips";
					$itemTitle = "Tooltips";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $tooltipsHTML;
					$itemCSS = $tooltipsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "alertModals";
					$itemTitle = "Alert Modals";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $alertModalsHTML;
					$itemCSS = $alertModalsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "icons";
					$itemTitle = "Icons";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $iconsHTML;
					$itemCSS = $iconsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "animations";
					$itemTitle = "Animations";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $animationsHTML;
					$itemCSS = $animationsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

				exitSection();




				/**********************************
				LAYOUT SECTION
				***********************************/

				$sectionID = "layout";
				$sectionTitle = "Layout";
				$sectionDescription = "The examples in this category are all about the layout of the website and content.";
				createSection($sectionID, $sectionTitle, $sectionDescription);

					$itemID = "grid";
					$itemTitle = "Responsive layout/Grid layout";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $gridHTML;
					$itemCSS = $gridCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "breadcrumbs";
					$itemTitle = "Breadcrumbs";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $breadcrumbsHTML;
					$itemCSS = $breadcrumbsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "gridLists";
					$itemTitle = "Grid Lists of content, media, or photos";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $gridListsHTML;
					$itemCSS = $gridListsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "verticalLists";
					$itemTitle = "Vertical Lists";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $verticalListsHTML;
					$itemCSS = $verticalListsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "cards";
					$itemTitle = "Cards";
					$itemDescription = "These are sample cards that can be used to show data and options.";
					$itemHTML = $cardsHTML;
					$itemCSS = $cardsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

				exitSection();




				/**********************************
				COMPONENTS SECTION
				***********************************/

				$sectionID = "components";
				$sectionTitle = "Components";
				$sectionDescription = "These are components that can be used on the website.";
				createSection($sectionID, $sectionTitle, $sectionDescription);

					$itemID = "buttons";
					$itemTitle = "Buttons";
					$itemDescription = "The official buttons come in 3 sizes: small, medium, and large.";
					$itemHTML = $buttonsHTML;
					$itemCSS = $buttonsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "buttonGroups";
					$itemTitle = "Button Groups";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $buttonGroupsHTML;
					$itemCSS = $buttonGroupsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "tables";
					$itemTitle = "Tables";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $tablesHTML;
					$itemCSS = $tablesCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "dialogs";
					$itemTitle = "Dialogs";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $dialogsHTML;
					$itemCSS = $dialogsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "navMenus";
					$itemTitle = "Navigational Menus";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $navMenusHTML;
					$itemCSS = $navMenusCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "dateTimePickers";
					$itemTitle = "Date and Time Pickers";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $dateTimePickersHTML;
					$itemCSS = $dateTimePickersCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "progressBar";
					$itemTitle = "Progress Bar";
					$itemDescription = "A progress bar for as the user moves through the steps. A full demonstration with interactivity is shown at <a href='http://codepen.io/alisonhall/pen/aNKNQJ'>http://codepen.io/alisonhall/pen/aNKNQJ</a>";
					$itemHTML = $progressBarHTML;
					$itemCSS = $progressBarCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "progressIndicators";
					$itemTitle = "Progress and Loading Indicators";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $progressIndicatorsHTML;
					$itemCSS = $progressIndicatorsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "checkboxes";
					$itemTitle = "Checkboxes";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $checkboxesHTML;
					$itemCSS = $checkboxesCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "radioButtons";
					$itemTitle = "Radio Buttons";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $radioButtonsHTML;
					$itemCSS = $radioButtonsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "dropDownMenus";
					$itemTitle = "Drop-down Menus";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $dropDownMenusHTML;
					$itemCSS = $dropDownMenusCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "sliders";
					$itemTitle = "Sliders";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $slidersHTML;
					$itemCSS = $slidersCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "switches";
					$itemTitle = "On-Off Switches";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $switchesHTML;
					$itemCSS = $switchesCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "steppers";
					$itemTitle = "Numeric-Input Steppers / Incrementers";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $steppersHTML;
					$itemCSS = $steppersCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "formFields";
					$itemTitle = "Form Fields";
					$itemDescription = "These are some sample form fields.";
					$itemHTML = $formFieldsHTML;
					$itemCSS = $formFieldsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "tabs";
					$itemTitle = "Tabs";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $tabsHTML;
					$itemCSS = $tabsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "toolbars";
					$itemTitle = "Toolbars";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $toolbarsHTML;
					$itemCSS = $toolbarsCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

					$itemID = "tokens";
					$itemTitle = "Tokens/Chips";
					$itemDescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia reiciendis, incidunt quos quo praesentium fugit illum minus, eos tempore repellat eligendi maxime blanditiis aliquid eaque, vitae neque vero similique officia?";
					$itemHTML = $tokensHTML;
					$itemCSS = $tokensCSS;
					createItem($itemID, $itemTitle, $itemDescription, $itemHTML, $itemCSS);

				exitSection();
			?>

		</section>
		<?php include("sidebar.php"); ?>

<?php include("footer.php") ?>