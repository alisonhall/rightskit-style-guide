<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="The front-end style guide for Rights.Kit">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Rights.Kit Front-End Style Guide</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link href='https://fonts.googleapis.com/css?family=Lato:300,100' rel='stylesheet' type='text/css'>
        <!-- <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Quattrocento+Sans:400,400italic,700,700italic|Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'> -->
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/xrayhtml.css">
        <link rel="stylesheet" href="css/prism.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/styleguide.css">
    </head>
    <body>