<?php 

$coloursHTML = '
<div class="colour colour1">#222222</div>
<div class="colour colour2">#FFFFFF</div>
<div class="colour colour3">#7D746F</div>
<div class="colour colour4">#BA274B</div>
<div class="colour colour5">#A0C7E4</div>

';
$coloursCSS = '
.colour {
  width: 200px;
  height: 200px;
  border: 1px solid black;
  display: inline-block;
  text-align: center;
  font-size: 2em;
}

.colour1 {
  background-color: #222222;
  color: white;
}

.colour2 {
  background-color: #FFFFFF;
}

.colour3 {
  background-color: #7D746F;
}

.colour4 {
  background-color: #BA274B;
}

.colour5 {
  background-color: #A0C7E4;
}
';

$coloursLighterHTML = '
<div class="colour colour1light">#6E6E6E</div>
<div class="colour colour2light">#FFFFFF</div>
<div class="colour colour3light">#EDEDED</div>
<div class="colour colour4light">#DE99AA</div>
<div class="colour colour5light">#D7EBFA</div>

';
$coloursLighterCSS = '
.colour1light {
  background-color: #6E6E6E;
  color: white;
}

.colour2light {
  background-color: #FFFFFF;
}

.colour3light {
  background-color: #EDEDED;
}

.colour4light {
  background-color: #DE99AA;
}

.colour5light {
  background-color: #D7EBFA;
}
';

$coloursDarkerHTML = '
<div class="colour colour1dark">#000000</div>
<div class="colour colour2dark">#B0B0B0</div>
<div class="colour colour3dark">#4A413D</div>
<div class="colour colour4dark">#751A30</div>
<div class="colour colour5dark">#5A8AAD</div>
<div class="colour colour5dark2">#3E6785</div>

';
$coloursDarkerCSS = '
.colour1dark {
  background-color: #000000;
  color: white;
}

.colour2dark {
  background-color: #B0B0B0;
}

.colour3dark {
  background-color: #4A413D;
}

.colour4dark {
  background-color: #751A30;
}

.colour5dark {
  background-color: #5A8AAD;
}

.colour5dark2 {
  background-color: #3E6785;
}
';

$typefaceHTML = '
<h1>h1: The quick brown fox jumps over the lazy dog</h1>
<h2>h2: The quick brown fox jumps over the lazy dog</h2>
<h3>h3: The quick brown fox jumps over the lazy dog</h3>
<p>Body text: Regular, 1em. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec sed odio dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
<p style="font-weight: bold;">Body text: Bold, 1em. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec sed odio dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
<p style="font-style: italic;">Body text: Italic, 1em. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec sed odio dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
';
$typefaceCSS = "
// <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

@font-face {
  font-family: 'Slim Joe';
  src: url('../fonts/BigJohnSlimJoe/SlimJoe.otf') format('opentype');
  font-weight: 400;
  font-style: normal;
}

body {
  font-family: 'Source Sans Pro', sans-serif;
  font-weight: 300;
  font-size: 1em;
}

h1 {
  font-family: 'Slim Joe', sans-serif;
  font-size: 2.5em;
}

h2 {
  font-family: 'Source Sans Pro', sans-serif;
  font-size: 1.8em;
  font-weight: 300;
}

h3 {
  font-family: 'Source Sans Pro', sans-serif;
  font-size: 1.3em;
  font-weight: 300;
}
";


$tooltipsHTML = '';
$tooltipsCSS = '';
$alertModalsHTML = '';
$alertModalsCSS = '';
$iconsHTML = '';
$iconsCSS = '';
$animationsHTML = '';
$animationsCSS = '';


$gridHTML = '';
$gridCSS = '';
$breadcrumbsHTML = '';
$breadcrumbsCSS = '';
$gridListsHTML = '';
$gridListsCSS = '';
$verticalListsHTML = '';
$verticalListsCSS = '';

$cardsHTML = '
<div class="card-container">
	<div class="card">
		<p>Etiam porta sem malesuada magna mollis euismod. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
		<button class="btn btn-big">Click here</button>
	</div>
	<div class="card">
		<p>Etiam porta sem malesuada magna mollis euismod. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
		<button class="btn btn-big">Click here</button>
	</div>
</div>
';
$cardsCSS = '
.card-container {
  width: 100%;
  text-align: center;
}

.card {
  display: inline-block;
  width: 25%;
  min-width: 240px;
  min-height: 300px;
  background-color: #EDEDED;
  padding: 20px;
  margin: 20px;
  text-align: center;
}

.card p {
  font-size: 1.3em;
}
';


$buttonsHTML = '
<button class="btn btn-big">Big Button</button>
<button class="btn btn-medium">Medium Button</button>
<button class="btn btn-small">Small Button</button>
';
$buttonsCSS = '
.btn {
  border: 1px solid #4cae4c;
  background-color: #5A8AAD;
  color: white;
  border-radius: 4px;
  padding: 0 12px;
  margin: 10px;
}

.btn:hover, .btn:active, .btn:focus {
  background-color: #3E6785;
}

.btn-big {
  font-size: 1.5em;
  width: 220px;
  height: 63px;
}

.btn-medium {
  font-size: 1.2em;
  width: 180px;
  height: 50px;
}

.btn-small {
  font-size: 1em;
  width: 150px;
  height: 40px;
}
';

$buttonGroupsHTML = '';
$buttonGroupsCSS = '';
$tablesHTML = '';
$tablesCSS = '';
$dialogsHTML = '';
$dialogsCSS = '';
$navMenusHTML = '';
$navMenusCSS = '';
$dateTimePickersHTML = '';
$dateTimePickersCSS = '';

$progressBarHTML = '
<div class="progress_bar_container">
	<div class="progress_bar">
		<hr class="all_steps">
		<hr class="current_steps">
		<div class="step current" id="step1" data-step="1">
			Payee Information
		</div>
		<div class="step" id="step2" data-step="2">
			Payment Information
		</div>
		<div class="step" id="step3" data-step="3">
			Contact Information
		</div>
		<div class="step" id="step4" data-step="4">
			Artist/Group Information
		</div>
		<div class="step" id="step5" data-step="5">
			Identification
		</div>
	</div>

	<div id="progress_bar_blocks">
		<div class="block" id="block1" style="left: 0%;">
			<div class="wrap">
				<h2>Block 1</h2>
				<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Duis mollis, est non commodo luctus, nisi
					erat porttitor ligula, eget lacinia odio sem nec elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				<br />
				<button onclick="step_process(1, 2)" class="btn btn-small">Next</button>
			</div>
		</div>
		<div class="block" id="block2">
			<div class="wrap">
				<h2>Block 2</h2>
				<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Duis mollis, est non commodo luctus, nisi
					erat porttitor ligula, eget lacinia odio sem nec elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				<br />
				<button onclick="step_process(2, 3)" class="btn btn-small">Next</button>
			</div>
		</div>
		<div class="block" id="block3">
			<div class="wrap">
				<h2>Block 3</h2>
				<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Duis mollis, est non commodo luctus, nisi
					erat porttitor ligula, eget lacinia odio sem nec elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				<br />
				<button onclick="step_process(3, 2)" class="btn btn-small">Prev</button>
				<button onclick="step_process(3, 4)" class="btn btn-small">Next</button>
			</div>
		</div>
		<div class="block" id="block4">
			<div class="wrap">
				<h2>Block 4</h2>
				<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Duis mollis, est non commodo luctus, nisi
					erat porttitor ligula, eget lacinia odio sem nec elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				<br />
				<button onclick="step_process(4, 5)" class="btn btn-small">Next</button>
			</div>
		</div>
		<div class="block" id="block5">
			<div class="wrap">
				<h2>Block 5</h2>
				<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Duis mollis, est non commodo luctus, nisi
					erat porttitor ligula, eget lacinia odio sem nec elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				<br /> Use the progress bar at the top as a navigation back.
			</div>
		</div>
	</div>
</div>
';

$progressBarCSS = "
.progress_bar_container {
  width: 90%;
  margin: 0 auto;
  padding-bottom: 10%;
  overflow: hidden;
  height: 100%;
}

.clear {
  clear: both;
}

.progress_bar {
  width: 80%;
  margin: 115px auto 0;
  position: relative;
  height: 80px;
  z-index: 10;
}
.progress_bar hr.all_steps {
  width: 80%;
  height: 7px;
  border: none;
  background: #DDDDDD;
  border-bottom: 1px solid #fff;
  position: absolute;
  bottom: 10px;
  left: 10%;
  z-index: 1;
}
.progress_bar hr.current_steps {
  width: 0%;
  border: 0;
  height: 5px;
  background: #A0C7E4;
  position: absolute;
  bottom: 12px;
  left: 10%;
  z-index: 3;
}
.progress_bar div.step {
  float: left;
  width: 20%;
  height: 80px;
  text-align: center;
  color: #ccc;
  position: relative;
  text-shadow: 1px 1px #fff;
  -webkit-transition: all 0.3s ease-in;
  -moz-transition: all 0.3s ease-in;
  -ms-transition: all 0.3s ease-in;
  -o-transition: all 0.3s ease-in;
  transition: all 0.3s ease-in;
}
.progress_bar div.step:before {
  position: absolute;
  width: 16px;
  height: 16px;
  border-radius: 20px;
  border: 2px solid transparent;
  background: #A0C7E4;
  bottom: 14px;
  left: 50%;
  margin-left: -6px;
  content: '';
  z-index: 4;
  display: none;
}
.progress_bar div.step:after {
  position: absolute;
  width: 20px;
  height: 20px;
  border-radius: 20px;
  border: 2px solid #DDDDDD;
  background: #DDDDDD;
  bottom: 12px;
  left: 50%;
  margin-left: -8px;
  content: '';
  z-index: 2;
}
.progress_bar div.step.current {
  color: #222;
}
.progress_bar div.step.current:before {
  display: block;
}
.progress_bar div.step.complete {
  color: #888;
  cursor: pointer;
}
.progress_bar div.step.complete:before {
  display: block;
}
.progress_bar div.step.complete:hover {
  color: #555;
}

#progress_bar_blocks {
  width: 100%;
  position: relative;
  height: 200px;
}
#progress_bar_blocks .block {
  position: absolute;
  width: 100%;
  left: 100%;
  height: 200px;
}
#progress_bar_blocks .block .wrap {
  width: 80%;
  margin: 0 auto;
}
";

$progressIndicatorsHTML = '';
$progressIndicatorsCSS = '';
$checkboxesHTML = '';
$checkboxesCSS = '';
$radioButtonsHTML = '';
$radioButtonsCSS = '';
$dropDownMenusHTML = '';
$dropDownMenusCSS = '';
$slidersHTML = '';
$slidersCSS = '';
$switchesHTML = '';
$switchesCSS = '';
$steppersHTML = '';
$steppersCSS = '';

$formFieldsHTML = '
<form action="" method="">
 	<fieldset>
 		<legend>Text input fields:</legend>
	 	<input type="text" name="lastname" value="Text 1" placeholder="Placeholder text">
	 	<input type="text" name="lastname" value="Text 2" placeholder="Placeholder text">
	 	<input type="text" name="lastname" placeholder="Placeholder text">
	 	<input type="text" name="lastname" placeholder="Placeholder text">
	</fieldset>
	<fieldset>
		<legend>Radio buttons:</legend>
	 	<input type="radio" name="radioGroupName" value="option1" checked> Option 1<br>
	 	<input type="radio" name="radioGroupName" value="option2"> Option 2<br>
	 	<input type="radio" name="radioGroupName" value="option3"> Option 3
 	</fieldset>
	<fieldset>
		<legend>Checkboxes:</legend>
		<input type="checkbox" name="option1" value="option1"> Option 1<br>
		<input type="checkbox" name="option2" value="option2"> Option 2
	</fieldset>
	<fieldset>
		<legend>Select options:</legend>
		<label for="categoryName">Select Label:</label>
		<select name="categoryName">
			<option value="option1">Option 1</option>
			<option value="option2">Option 2</option>
			<option value="option3">Option 3</option>
			<option value="option4">Option 4</option>
		</select>
	</fieldset>
	<fieldset>
		<legend>Textarea:</legend>
		<label for="message">Textarea Label:</label><br>
		<textarea name="message" rows="5" cols="60">Sem Mollis Quam Tellus Ornare.</textarea>
	</fieldset>
	<fieldset>
		<legend>Form Buttons:</legend>
		<div class="form-buttons-container">
			<button type="submit" value="Submit" class="btn btn-medium">Submit</button>
			<button type="reset" value="Reset" class="btn btn-medium">Reset</button>
		</div>
	</fieldset>
 </form>
';
$formFieldsCSS = '
.form-buttons-container {
  width: 100%;
  text-align: center;
}

form {
  max-width: 800px;
  background-color: #EDEDED;
  padding: 20px;
  margin: 20px;
}

form fieldset {
  margin: 5px 0;
}

form legend {
  color: #5A8AAD;
  font-weight: bold;
}

form input[type="text"] {
  min-width: 200px;
  margin: 5px;
  padding: 5px 10px;
}

form select {
  min-width: 200px;
  height: 40px;
}
';

$tabsHTML = '';
$tabsCSS = '';
$toolbarsHTML = '';
$toolbarsCSS = '';
$tokensHTML = '';
$tokensCSS = '';



 ?>

