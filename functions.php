<?php 

	function buildSidebar($temp){
		global $sidebar;
		$sidebar .= $temp;
	}

	function createSection($idName, $h2Name, $sectionDescription){
		echo '<section id="' . $idName . '" class="section">';
		echo '<h2>' . $h2Name . '</h2>';
		echo '<div class="sectionDescription">' . $sectionDescription . '</div>';
		buildSidebar('<li><a href="#' . $idName . '">' . $h2Name . '</a><ul>');
	}

	function exitSection(){
		echo '</section>';
		buildSidebar('</ul></li>');
	}

	function createItem($idName, $h3Name, $itemDescription, $itemHTML, $itemCSS){
		if($itemHTML != "") {
			echo '<section id="' . $idName . '" class="item">';
			echo '<h3>' . $h3Name . '</h3>';
			echo '<div class="itemDescription">' . $itemDescription . '</div>';
			echo '<div class="itemExamples" data-xrayhtml>' . $itemHTML . '</div>';
			echo '<button class="toggleShow">Show/Hide CSS</button>';
			echo '<div class="cssCode">';
			echo '<div class="cssTitle">CSS</div><pre class="language-css"><code>' . $itemCSS . '</code></pre></div>';
			echo '</section>';
			buildSidebar('<li><a href="#' . $idName . '">' . $h3Name . '</a></li>');
		}
	}

 ?>